/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

export default [
  {
    path: '/',
    component: require('./components/pages/Home.vue'),
    name: 'home'
  },
  {
    path: '/signin',
    component: require('./components/pages/Signin.vue'),
    name: 'signin'
  },
  {
    path: '/signup',
    component: require('./components/pages/Signup.vue'),
    name: 'signup'
  },
  {
    path: '/signout',
    component: require('./components/pages/Signout.vue'),
    name: 'signout'
  },
  {
    path: '/profile',
    component: require('./components/pages/Profile.vue'),
    name: 'profile',
    meta: { requiresAuth: true }
  },
  {
    path: '/blocks',
    component: require('./components/pages/Blocks.vue'),
    name: 'blocks',
    meta: { requiresAuth: true }
  },
  {
    path: '/brawl',
    component: require('./components/pages/Brawl.vue'),
    name: 'brawl',
    meta: { requiresAuth: true }
  },
  {
    path: '/store',
    component: require('./components/pages/Store.vue'),
    name: 'store',
    meta: { requiresAuth: true }
  },
  {
    path: '/error403',
    component: require('./components/pages/Error403.vue'),
    name: 'error403'
  },
  {
    path: '/error401',
    component: require('./components/pages/Error401.vue'),
    name: 'error401'
  },
  {
    path: '/error500',
    component: require('./components/pages/Error500.vue'),
    name: 'error500'
  },
  {
    path: '*',
    component: require('./components/pages/Error404.vue'),
    name: 'error404'
  }
]
