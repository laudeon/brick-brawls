/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

export default {
  load: (vm) => {
    // Global router hooks
    // Protected routes
    vm.$router.beforeEach((to, from, next) => {
      if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!vm.isAuth()) {
          next({
            path: '/signin',
            query: { redirect: to.fullPath }
          })
        } else {
          next()
        }
      } else {
        next()
      }
    })
  }
}
