/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

/* eslint-disable */

import Blockly from 'Blockly'

let weapons
let armors

// @param Object user
let setUserInfo = function (user) {
  weapons = user.brick.weapons.reduce((origin, weapon) => {
    origin.push([weapon.name, weapon.name])
    return origin
  }, [])
  armors = user.brick.armors.reduce((origin, armor) => {
    origin.push([armor.name, armor.name])
    return origin
  }, [])
}

let initCustomBlocks = function () {
  window.LoopTrap = 1000
  Blockly.JavaScript.INFINITE_LOOP_TRAP = "if(--window.LoopTrap == 0) throw 'Infinite loop.';\n"

  Blockly.Blocks['start'] = {
    init: function () {
      this.jsonInit({
        'type': 'event_start',
        'message0': 'Round start',
        'nextStatement': null,
        'colour': '#d35400'
      })
    }
  }

  Blockly.JavaScript['start'] = function (block) {
    let code = ";\n"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['hasWeapon'] = {
    init: function () {
      this.jsonInit({
        'type': 'have_weapon',
        'message0': 'I have a weapon',
        'output': 'Boolean'
      })
    }
  }

  Blockly.JavaScript['hasWeapon'] = function (block) {
    let code = "brick.hasWeapon()"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['getWeapon'] = {
    init: function () {
      this.jsonInit({
        'type': 'equip_weapon',
        'message0': 'Equip %1 weapon',
        'args0': [
          {
            'type': 'field_dropdown',
            'name': 'WEAPON',
            'options': weapons
          }
        ],
        'previousStatement': null,
        'nextStatement': null
      })
    }
  }

  Blockly.JavaScript['getWeapon'] = function (block) {
    let weapon = block.getFieldValue('WEAPON')
    let code = "brick.getWeapon('" + weapon + "');\n"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['hasArmor'] = {
    init: function () {
      this.jsonInit({
        'type': 'have_armor',
        'message0': 'I have an armor',
        'output': 'Boolean'
      })
    }
  }

  Blockly.JavaScript['hasArmor'] = function (block) {
    let code = "brick.hasArmor()"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['getArmor'] = {
    init: function () {
      this.jsonInit({
        'type': 'equip_armor',
        'message0': 'Equip %1 armor',
        'args0': [
          {
            'type': 'field_dropdown',
            'name': 'ARMOR',
            'options': armors
          }
        ],
        'previousStatement': null,
        'nextStatement': null
      })
    }
  }

  Blockly.JavaScript['getArmor'] = function (block) {
    let armor = block.getFieldValue('ARMOR')
    let code = "brick.getArmor('" + armor + "');\n"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['getActionPoints'] = {
    init: function () {
      this.jsonInit({
        'type': 'get_ap',
        'message0': 'Get current action points',
        'output': 'Number'
      })
    }
  }

  Blockly.JavaScript['getActionPoints'] = function (block) {
    let code = "brick.getActionPoints()"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['getMouvementPoints'] = {
    init: function () {
      this.jsonInit({
        'type': 'get_mp',
        'message0': 'Get current mouvement points',
        'output': 'Number'
      })
    }
  }

  Blockly.JavaScript['getMouvementPoints'] = function (block) {
    let code = "brick.getMouvementPoints()"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['move'] = {
    init: function () {
      this.jsonInit({
        'type': 'move',
        'message0': 'Move %1 enemy',
        'args0': [
          {
            'type': 'field_dropdown',
            'name': 'MOVE',
            'options': [
              [ 'forward', 'forward' ],
              [ 'backward', 'backward' ]
            ]
          }
        ],
        'previousStatement': null,
        'nextStatement': null
      })
    }
  }

  Blockly.JavaScript['move'] = function (block) {
    let direction = block.getFieldValue('MOVE')
    let code = "brick.move('" + direction + "');\n"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['attack'] = {
    init: function () {
      this.jsonInit({
        'type': 'attack',
        'message0': 'Attack enemy',
        'previousStatement': null,
        'nextStatement': null
      })
    }
  }

  Blockly.JavaScript['attack'] = function (block) {
    let code = "brick.shot();\n"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }

  Blockly.Blocks['canAttack'] = {
    init: function () {
      this.jsonInit({
        'type': 'can_attack',
        'message0': 'I can attack enemy',
        'output': 'Boolean'
      })
    }
  }

  Blockly.JavaScript['canAttack'] = function (block) {
    let code = "brick.canShot()"

    return [code, Blockly.JavaScript.ORDER_MEMBER]
  }
}

export { Blockly, setUserInfo, initCustomBlocks }
