/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

import { Blockly, setUserInfo, initCustomBlocks } from './blockly-config'

let workspace = null

export default {
  methods: {
    // Blockly
    // Initialize Blockly on document events
    // Resize Blockly on window reize event
    initBlockly (user) {
      initCustomBlocks()
      setUserInfo(user)

      let gridOptions = {
        spacing: 20,
        length: 20,
        colour: '#ccc',
        snap: true
      }

      workspace = Blockly.inject('google-block', {
        toolbox: document.getElementById('toolbox'),
        grid: gridOptions,
        trashcan: true
      })

      window.addEventListener('resize', this.onresize, false)
      this.onresize()
      Blockly.svgResize(workspace)
    },
    // Handle Blockly resizing
    onresize () {
      let blocklyArea = document.getElementById('block')
      let blocklyDiv = document.getElementById('google-block')
      // Compute the absolute coordinates and dimensions of blocklyArea.
      let element = blocklyArea
      let x = 0
      let y = 0
      do {
        x += element.offsetLeft
        y += element.offsetTop
        element = element.offsetParent
      } while (element)
      // Position blocklyDiv over blocklyArea.
      blocklyDiv.style.left = x + 'px'
      blocklyDiv.style.top = y + 'px'
      blocklyDiv.style.width = blocklyArea.offsetWidth + 'px'
      blocklyDiv.style.height = (blocklyArea.offsetHeight - 50) + 'px'
    },
    // Resotre user's brick AI from the stored string
    restoreBlockly (xmlText) {
      let xml = Blockly.Xml.textToDom(xmlText)
      Blockly.Xml.domToWorkspace(xml, workspace)
    },
    // Save the JS string and the XML string
    compileBlockly () {
      let code
      let xml
      let xmlText

      Blockly.JavaScript.INFINITE_LOOP_TRAP = null

      code = Blockly.JavaScript.workspaceToCode(workspace).replace(new RegExp(',1.2', 'g'), '')
      xml = Blockly.Xml.workspaceToDom(workspace)
      xmlText = Blockly.Xml.domToText(xml)

      this.$parent.user.ai = {
        js: code,
        xml: xmlText
      }

      this
        .putResource('api/users/' + this.$parent.user._id, this.$parent.user)
        .then(res => {
          this.$localStorage.set('user', res.user)
          this.$parent.user = res.user
        })
        .catch(err => {
          this.errors = err.body.message.map(elem => elem.message || elem)
        })
    },
    testBrawl () {

    }
  }
}
