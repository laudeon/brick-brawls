/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

import Vue from 'vue'
import store from './store'
import routes from './router/routes'
import security from './security'
import VueResource from 'vue-resource'
import VueLocalStorage from 'vue-localstorage'
import VueRouter from 'vue-router'
import configRouter from './router/router-config'
import configResource from './resource/resource-config'

Vue.use(VueResource)
Vue.use(VueLocalStorage)
Vue.use(VueRouter)

const router = new VueRouter({
  routes: routes
})

var vm = new Vue({
  mixins: [security],
  data: {
    user: false,
    token: false
  },
  localStorage: {
    user: { type: Object },
    token: { type: String }
  },
  store,
  router,
  created () {
    this.user = this.$localStorage.get('user')
    this.token = this.$localStorage.get('token')
  }
})

configRouter.load(vm)
configResource.load(Vue, vm)

vm.$mount('#app')
