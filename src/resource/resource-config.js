/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

export default {
  load: (Vue, vm) => {
    // Global error handler
    Vue.http.interceptors.push((request, next) => {
      if (vm.isAuth()) {
        request.headers.set('authorization', 'JWT ' + vm.getToken())
      }
      next(res => {
        switch (res.status) {
          case 403:
            vm.$router.push('/error403')
            break
          case 401:
            vm.$router.push('/error401')
            break
          case 404:
            vm.$router.push('/error404')
            break
          case 500:
            vm.$router.push('/error500')
            break
          default:
            // must be handle directly in the catch method
            break
        }
      })
    })
  }
}
