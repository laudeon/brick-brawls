/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

const APIBasePath = 'http://localhost:3000/'

export default {
  methods: {
    getResource (url) {
      return this.$http
                 .get(APIBasePath + url + '')
                 .then(res => res.json())
    },
    postResource (url, data = {}) {
      return this.$http
                 .post(APIBasePath + url + '', data)
                 .then(res => res.json())
    },
    putResource (url, data = {}) {
      return this.$http
                 .put(APIBasePath + url + '', data)
                 .then(res => res.json())
    },
    deleteResource (url) {
      return this.$http
                 .delete(APIBasePath + url + '')
                 .then(res => res.json())
    }
  }
}
