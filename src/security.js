/*!
 * Brick Brawls Front-end
 * 2016 laudeon <audeon.louis@gmail.com>
 * MIT Licensed
 */

export default {
  methods: {
    isAuth () {
      return this.user
    },
    getToken () {
      return this.token
    },
    signout () {
      this.$localStorage.remove('user')
      this.$localStorage.remove('token')
      this.user = false
      this.token = false
      this.$router.push('/')
    }
  }
}
