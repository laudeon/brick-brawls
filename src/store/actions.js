import * as types from './mutation-types'

export const fight = ({ commit }, user, enemy) => {
  commit(types.FIGTH, {
    user: user,
    enemy: enemy
  })
}
